<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/welcome', function () {
//     return view('welcome');
// });

Route::get('admin/login', [\App\Http\Controllers\Auth\AdminLoginController::class, 'showLoginForm']);
Route::post('admin/login', [\App\Http\Controllers\Auth\AdminLoginController::class, 'login']);
Route::get('admin/logout', [\App\Http\Controllers\Auth\AdminLoginController::class, 'logout']);

Route::group(['prefix' => 'admin','middleware' => 'assign.guard:admin,admin/login'],function(){
    Route::get('/dashboard', [\App\Http\Controllers\Admin\DashboardController::class, 'index']);
    Route::resource('/blog-category', \App\Http\Controllers\Admin\BlogCategoryController::class)->name('index','blog-category');
    Route::resource('/blog-post', \App\Http\Controllers\Admin\BlogPostController::class)->name('index', 'blog-post');
    Route::resource('/portfolio-project', \App\Http\Controllers\Admin\PortfolioProjectController::class)->name('index', 'portfolio-project');
    Route::get('/setting', [\App\Http\Controllers\Admin\SettingController::class, 'index'])->name('setting');
    Route::put('/setting', [\App\Http\Controllers\Admin\SettingController::class, 'update'])->name('setting.update');
});
