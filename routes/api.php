<?php

use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\BlogCategoryController;
use App\Http\Controllers\BlogPostController;
use App\Http\Controllers\CallbackController;
use App\Http\Controllers\PortfolioProjectController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'auth:api'], function () {

});

Route::group(['middleware' => 'guest:api'], function () {
    Route::get('projects', [PortfolioProjectController::class, 'getProjects']);
    Route::get('project', [PortfolioProjectController::class, 'getProject']);
    Route::get('blog-posts', [BlogPostController::class, 'getPosts']);
    Route::get('blog-post', [BlogPostController::class, 'getPost']);
    Route::get('blog-categories', [BlogCategoryController::class, 'getCategories']);
    Route::post('send-message', [CallbackController::class, 'sendMessage']);
//    Route::post('register', [RegisterController::class, 'register']);
});
