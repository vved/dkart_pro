@extends('admin.layouts.app')

@section('title', 'admin.setting.create_post')

@section('content')
  <!-- BEGIN FORM -->
  <section>
    <div class="section-header">
      <ol class="breadcrumb">
        <li class="active">@lang('admin.setting.title')</li>
      </ol>
    </div>
    <div class="section-body contain-lg">
      <form class="form" method="post" enctype="multipart/form-data" action="{{ route('setting.update') }}">
      @csrf
      @method('PUT')

      <!-- BEGIN FIELDS -->
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-body">

                <div class="form-group floating-label">
                  <input type="text" class="form-control" id="email" name="email" value="{{ isset($setting['email']->value)?$setting['email']->value:'' }}">
                  <label for="email">@lang('admin.setting.email')</label>
                </div>

                <div class="form-group floating-label">
                  <input type="text" class="form-control" id="telephone" name="telephone" value="{{ isset($setting['telephone']->value)?$setting['telephone']->value:'' }}">
                  <label for="telephone">@lang('admin.setting.telephone')</label>
                </div>

              </div><!--end .card-body -->
            </div><!--end .card -->
          </div><!--end .col -->
        </div>

        <button class="btn ink-reaction btn-raised btn-primary">@lang('admin.submit')</button>
      </form>

    </div><!--end .section-body -->
  </section>
  <!-- END FORM -->
@endsection
@push('script')
  <script>
    $('body').delegate('.image_input','change', function () {
      var input = this;
      if (input.files && input.files[0]) {
        var key = parseInt($(input).attr('data-key')) + 1;
        var reader = new FileReader();
        reader.onload = function (e) {
          $(input).parent().siblings('.image_file').html('<img src="' + e.target.result + '" alt="image">');
          $(input).siblings('span').css('display', 'none');
          $(input).parent().removeClass('upload-file-container-text');
        };
        reader.readAsDataURL(input.files[0]);
      }
    });

    $('body').delegate('.delete button','click', function (e) {
      e.preventDefault();
      console.log($(this).parent().parent().find('.image_input'))
      $(this).parent().parent().find('.image_input').val('');
      $(this).parent().parent().find('.image_file img').remove();
      $(this).parent().parent().find('.block-add-image').addClass('upload-file-container-text');
      $(this).parent().parent().find('.block-add-image span').css('display', 'block');
    });
  </script>
@endpush
