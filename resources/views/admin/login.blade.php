@extends('admin.layouts.guest')

@section('title', 'Login')

@section('content')
  <!-- BEGIN LOGIN SECTION -->
  <section class="section-account">
    <div class="img-backdrop" style="background-image: url('/assets/img/img16.jpg')"></div>
    <div class="spacer"></div>
    <div class="card contain-sm style-transparent">
      <div class="card-body">
        <div class="row">
          <div class="col-sm-offset-3 col-sm-6">
            <br/>
            <span class="text-lg text-bold text-primary">DKart</span>
            <br/><br/>
            <form class="form floating-label" method="POST" action="/admin/login">
              {!! csrf_field() !!}
              <div class="form-group">
                <input type="email" name="email" value="{{ old('email') }}" class="form-control">
                <label for="email">Email</label>
              </div>
              <div class="form-group">
                <input type="password" class="form-control" id="password" name="password">
                <label for="password">Password</label>
                <p class="help-block"><a href="#">Forgotten?</a></p>
              </div>
              <br/>
              <div class="row">
                <div class="col-xs-6 text-left">
                  <div class="checkbox checkbox-inline checkbox-styled">
                    <label>
                      <input  type="checkbox" name="remember"> <span>Remember me</span>
                    </label>
                  </div>
                </div><!--end .col -->
                <div class="col-xs-6 text-right">
                  <button class="btn btn-primary btn-raised" type="submit">Login</button>
                </div><!--end .col -->
              </div><!--end .row -->
            </form>
          </div><!--end .col -->
        </div><!--end .row -->
      </div><!--end .card-body -->
    </div><!--end .card -->
  </section>
  <!-- END LOGIN SECTION -->
@endsection
