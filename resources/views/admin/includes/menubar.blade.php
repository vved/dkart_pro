<div id="menubar" class="menubar-inverse ">
  <div class="menubar-fixed-panel">
    <div>
      <a class="btn btn-icon-toggle btn-default menubar-toggle" data-toggle="menubar" href="javascript:void(0);">
        <i class="fa fa-bars"></i>
      </a>
    </div>
    <div class="expanded">
      <a href="../../html/dashboards/dashboard.html">
        <span class="text-lg text-bold text-primary ">MATERIAL&nbsp;ADMIN</span>
      </a>
    </div>
  </div>
  <div class="menubar-scroll-panel">

    <!-- BEGIN MAIN MENU -->
    <ul id="main-menu" class="gui-controls">

      <!-- BEGIN DASHBOARD -->
      <li>
        <a href="../../html/dashboards/dashboard.html" class="active">
          <div class="gui-icon"><i class="md md-home"></i></div>
          <span class="title">Dashboard</span>
        </a>
      </li><!--end /menu-li -->
      <!-- END DASHBOARD -->

      <!-- BEGIN BLOG -->
      <li class="gui-folder">
        <a>
          <div class="gui-icon"><i class="fa fa-newspaper-o"></i></div>
          <span class="title">Blog</span>
        </a>
        <!--start submenu -->
        <ul>
          <li><a href="{{ route('blog-category') }}"><span class="title">Category</span></a></li>
          <li><a href="{{ route('blog-post') }}"><span class="title">Post</span></a></li>
        </ul><!--end /submenu -->
      </li><!--end /menu-li -->
      <!-- END BLOG -->

      <!-- BEGIN PORTFOLIO -->
      <li>
        <a href="{{ route('portfolio-project') }}" >
          <div class="gui-icon"><i class="md md-work"></i></div>
          <span class="title">Portfolio</span>
        </a>
      </li><!--end /menu-li -->
      <!-- END PORTFOLIO -->

      <!-- BEGIN SETTING -->
      <li>
        <a href="{{ route('setting') }}" >
          <div class="gui-icon"><i class="md md-settings"></i></div>
          <span class="title">Setting</span>
        </a>
      </li><!--end /menu-li -->
      <!-- END SETTING -->

    </ul><!--end .main-menu -->
    <!-- END MAIN MENU -->

    <div class="menubar-foot-panel">
      <small class="no-linebreak hidden-folded">
        <span class="opacity-75">Copyright &copy; {{ date('Y') }}</span> <strong>DKart.pro</strong>
      </small>
    </div>
  </div><!--end .menubar-scroll-panel-->
</div><!--end #menubar-->
