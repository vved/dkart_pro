<!DOCTYPE html>
<html lang="en">
<head>
  <title>Material Admin - Dashboard</title>

  <!-- BEGIN META -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="keywords" content="your,keywords">
  <meta name="description" content="Short explanation about this website">
  <!-- END META -->

  <!-- BEGIN STYLESHEETS -->
  <link href='http://fonts.googleapis.com/css?family=Roboto:300italic,400italic,300,400,500,700,900' rel='stylesheet' type='text/css'/>
  <link type="text/css" rel="stylesheet" href="/assets/css/theme-default/bootstrap.css?1422792965" />
  <link type="text/css" rel="stylesheet" href="/assets/css/theme-default/materialadmin.css?1425466319" />
  <link type="text/css" rel="stylesheet" href="/assets/css/theme-default/font-awesome.min.css?1422529194" />
  <link type="text/css" rel="stylesheet" href="/assets/css/theme-default/material-design-iconic-font.min.css?1421434286" />
  <link type="text/css" rel="stylesheet" href="/assets/css/theme-default/libs/rickshaw/rickshaw.css?1422792967" />
  <link type="text/css" rel="stylesheet" href="/assets/css/theme-default/libs/morris/morris.core.css?1420463396" />
  <link type="text/css" rel="stylesheet" href="/assets/css/theme-default/libs/select2/select2.css?1424887856" />
  <link type="text/css" rel="stylesheet" href="/assets/css/theme-default/libs/dropzone/dropzone-theme.css?1424887864" />
  <!-- END STYLESHEETS -->

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script type="text/javascript" src="/assets/js/libs/utils/html5shiv.js?1403934957"></script>
  <script type="text/javascript" src="/assets/js/libs/utils/respond.min.js?1403934956"></script>
  <![endif]-->
</head>
<body class="menubar-hoverable header-fixed ">

<!-- BEGIN HEADER-->
@include('admin.includes.header')
<!-- END HEADER-->

<!-- BEGIN BASE-->
<div id="base">

  <!-- BEGIN OFFCANVAS LEFT -->
  <div class="offcanvas">
  </div><!--end .offcanvas-->
  <!-- END OFFCANVAS LEFT -->

  @include('admin.includes.notification')

  <!-- BEGIN CONTENT-->
  <div id="content">
    @yield('content')
  </div><!--end #content-->
  <!-- END CONTENT -->

  <!-- BEGIN MENUBAR-->
  @include('admin.includes.menubar')
  <!-- END MENUBAR -->

  <!-- BEGIN OFFCANVAS RIGHT -->
  @include('admin.includes.canvas')
  <!-- END OFFCANVAS RIGHT -->

</div><!--end #base-->
<!-- END BASE -->

<!-- BEGIN JAVASCRIPT -->
<script src="/assets/js/libs/jquery/jquery-1.11.2.min.js"></script>
<script src="/assets/js/libs/jquery/jquery-migrate-1.2.1.min.js"></script>
<script src="/assets/js/libs/bootstrap/bootstrap.min.js"></script>
<script src="/assets/js/libs/spin.js/spin.min.js"></script>
<script src="/assets/js/libs/autosize/jquery.autosize.min.js"></script>
<script src="/assets/js/libs/moment/moment.min.js"></script>
<script src="/assets/js/libs/flot/jquery.flot.min.js"></script>
<script src="/assets/js/libs/flot/jquery.flot.time.min.js"></script>
<script src="/assets/js/libs/flot/jquery.flot.resize.min.js"></script>
<script src="/assets/js/libs/flot/jquery.flot.orderBars.js"></script>
<script src="/assets/js/libs/flot/jquery.flot.pie.js"></script>
<script src="/assets/js/libs/flot/curvedLines.js"></script>
<script src="/assets/js/libs/jquery-knob/jquery.knob.min.js"></script>
<script src="/assets/js/libs/sparkline/jquery.sparkline.min.js"></script>
<script src="/assets/js/libs/nanoscroller/jquery.nanoscroller.min.js"></script>
<script src="/assets/js/libs/d3/d3.min.js"></script>
<script src="/assets/js/libs/d3/d3.v3.js"></script>
<script src="/assets/js/libs/rickshaw/rickshaw.min.js"></script>
<script src="/assets/js/core/source/App.js"></script>
<script src="/assets/js/core/source/AppNavigation.js"></script>
<script src="/assets/js/core/source/AppOffcanvas.js"></script>
<script src="/assets/js/core/source/AppCard.js"></script>
<script src="/assets/js/core/source/AppForm.js"></script>
<script src="/assets/js/core/source/AppNavSearch.js"></script>
<script src="/assets/js/core/source/AppVendor.js"></script>
<script src="/assets/js/core/demo/Demo.js"></script>
<script src="/assets/js/libs/ckeditor/ckeditor.js"></script>
<script src="/assets/js/libs/ckeditor/adapters/jquery.js"></script>
<script src="/assets/js/core/source/FormEditors.js"></script>
<script src="/assets/js/libs/bootstrap-tagsinput/bootstrap-tagsinput.min.js"></script>
<script src="/assets/js/core/demo/Demo.js"></script>
<script src="/assets/js/libs/select2/select2.min.js"></script>
<!-- END JAVASCRIPT -->

<!-- BEGIN SCRIPTS-->
@stack('script')
<!-- END SCRIPTS-->

</body>
</html>
