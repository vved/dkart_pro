@extends('admin.layouts.app')

@section('title', 'admin.portfolio.title')

@section('content')
  <!-- BEGIN TABLE HOVER -->
  <section class="has-actions style-default-bright">
    <div class="section-body_div">
      <br>
      <h2 class="text-primary">@lang('admin.portfolio.title')
      <a href="{{ route('portfolio-project.create') }}" class="btn ink-reaction btn-raised btn-primary pull-right"><i class="md md-add"></i> New item</a>
      </h2>
      <br>
      <table class="table table-hover">
        <thead>
        <tr>
          <th>@lang('admin.portfolio.id')</th>
          <th>@lang('admin.portfolio.image')</th>
          <th>@lang('admin.portfolio.title_column')</th>
          <th>@lang('admin.portfolio.sort_order')</th>
          <th>@lang('admin.portfolio.status')</th>
          <th class="text-right">@lang('admin.portfolio.actions')</th>
        </tr>
        </thead>
        <tbody>
        @foreach($portfolioProjects as $portfolioProject)
        <tr>
          <td>{{ $portfolioProject->id }}</td>
          <td><img src="{{ isset($portfolioProject->portfolioImage->first()->image) ? $portfolioProject->portfolioImage->first()->image : 'placeholder.png' }}" alt="image" width="180"></td>
          <td>{{ isset($portfolioProject->portfolioDescription->first()->title)?$portfolioProject->portfolioDescription->first()->title:'' }}</td>
          <td>{{ $portfolioProject->sort_order }}</td>
          <td>{{ $portfolioProject->status }}</td>
          <td class="text-right">
            <div class="pull-right">
              <form method="POST" action="{{ route('portfolio-project.destroy', $portfolioProject->id) }}">
                @csrf
                @method('DELETE')
                <button type="submit" class="btn btn-icon-toggle" data-toggle="tooltip" data-placement="top" data-original-title="Delete row"><i class="fa fa-trash-o"></i></button>
              </form>
            </div>
            <a class="btn btn-icon-toggle pull-right" data-toggle="tooltip" data-placement="top" data-original-title="Edit row" href="{{ route('portfolio-project.edit', $portfolioProject->id) }}"><i class="fa fa-pencil"></i></a>
          </td>
        </tr>
        @endforeach
        </tbody>
      </table>
    </div><!--end .section-body -->
  </section>
  <!-- END TABLE HOVER -->
@endsection
