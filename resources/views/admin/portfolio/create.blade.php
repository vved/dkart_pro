@extends('admin.layouts.app')

@section('title', 'admin.portfolio.create_portfolio')

@section('content')
  <!-- BEGIN FORM -->
  <section>
    <div class="section-header">
      <ol class="breadcrumb">
        <li class="active">@lang('admin.portfolio.create_portfolio')</li>
      </ol>
    </div>
    <div class="section-body contain-lg">
      <form class="form" method="post" action="{{ route('portfolio-project.store') }}" enctype="multipart/form-data">
      @csrf

      <!-- BEGIN FIELDS -->
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-head">
                <ul class="nav nav-tabs" data-toggle="tabs">
                  @foreach($languages as $key_language => $language)
                    <li class="{{ $loop->first ? 'active' : '' }}"><a
                        href="#lang-{{ $key_language }}">{{ $language }}</a></li>
                  @endforeach
                </ul>
              </div><!--end .card-head -->
              <div class="card-body tab-content">
                @foreach($languages as $key_language => $language)
                  <div class="tab-pane {{ $loop->first ? 'active' : '' }}" id="lang-{{ $key_language }}">

                    <input type="hidden" name="{{ $key_language }}[language]" value="{{ $key_language }}">

                    <div class="form-group floating-label {{ $errors->has($key_language.'.title')? 'has-error' : '' }}">
                      <input type="text" class="form-control" id="{{ $key_language }}-title" name="{{ $key_language }}[title]" value="{{ Request::old($key_language.'.title') }}">
                      <label for="{{ $key_language }}-title">@lang('admin.portfolio.title_column')</label>
                      @if ($errors->has($key_language.'.title'))
                        <span class="text-danger">{{ $errors->first($key_language.'.title') }}</span>
                      @endif
                    </div>

                    <!-- BEGIN CKEDITOR - STANDARD -->
                    <h4>@lang('admin.portfolio.description')</h4>
                    <div class="form-group floating-label {{ $errors->has($key_language.'.description')? 'has-error' : '' }}">
                      <textarea class="form-control control-12-rows ckeditor" name="{{ $key_language }}[description]" placeholder="Enter text ...">{{ Request::old($key_language.'.description') }}</textarea>
                      @if ($errors->has($key_language.'.description'))
                        <span class="text-danger">{{ $errors->first($key_language.'.description') }}</span>
                      @endif
                    </div>
                    <!-- END CKEDITOR - STANDARD -->

                    <div class="form-group floating-label">
                      <input type="text" class="form-control" id="{{ $key_language }}-meta_title" name="{{ $key_language }}[meta_title]" value="{{ Request::old($key_language.'.meta_title') }}">
                      <label for="{{ $key_language }}-meta_title">@lang('admin.portfolio.meta_title')</label>
                    </div>

                    <div class="form-group floating-label">
                      <textarea class="form-control" id="{{ $key_language }}-meta_description" name="{{ $key_language }}[meta_description]">{{ Request::old($key_language.'.meta_description') }}</textarea>
                      <label for="{{ $key_language }}-meta_description">@lang('admin.portfolio.meta_description')</label>
                    </div>

                    <div class="form-group floating-label">
                      <input type="text" class="form-control" id="{{ $key_language }}-slag" name="{{ $key_language }}[slag]" value="{{ Request::old($key_language.'.slag') }}">
                      <label for="{{ $key_language }}-slag">@lang('admin.portfolio.slag')</label>
                    </div>

                    <div class="form-group floating-label">
                      <input type="text" class="form-control" id="{{ $key_language }}-keyword" name="{{ $key_language }}[keyword]" value="{{ Request::old($key_language.'.keyword') }}">
                      <label for="{{ $key_language }}-keyword">@lang('admin.portfolio.keyword')</label>
                    </div>

                  </div>
                @endforeach
              </div><!--end .card-body -->
            </div><!--end .card -->
          </div><!--end .col -->
        </div><!--end .row -->

        <!-- BEGIN FILE UPLOAD -->
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-head style-primary">
                <header>Images</header>
              </div>
              <div class="card-body no-padding">
                <div class="form-group col-sm-12 block_images">
                  <div class="col-sm-6">
                    <div class="upload-file-container preview_image">
                      <div class="image col-sm-4 text-center">
                        <div class="image_file"></div>
                        <div class="upload-file-container-text">
                          <span><i class="md md-add-to-photos"></i> Add image</span>
                          <input type="file" name="portfolio_project_image[0][image]" data-key="0" class="image_input"/>
                        </div>
                      </div>
                      <div class="sort col-sm-6">
                        <input type="text" name="portfolio_project_image[0][sort]" placeholder="sort">
                      </div>
                      <div class="delete col-sm-2" style="display: none">
                        <button type="submit" class="btn btn-icon-toggle" data-toggle="tooltip" data-original-title="Delete image"><i class="fa fa-trash-o"></i></button>
                      </div>
                    </div>
                  </div>
                </div>
              </div><!--end .card-body -->
            </div><!--end .card -->
          </div><!--end .col -->
        </div><!--end .row -->
        <!-- END FILE UPLOAD -->

        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-body">

                <div class="form-group floating-label">
                  <input type="text" class="form-control" id="sort_order" name="sort_order" value="{{ Request::old('sort_order') }}">
                  <label for="sort_order">@lang('admin.portfolio.sort_order')</label>
                </div>

                <div class="form-group">
                  <label for="sort_order">@lang('admin.portfolio.status')</label>
                  <select class="form-control" data-placeholder="Select an item" id="status" name="status">
                    <option value="1" {{ Request::old('status') == 1 ? 'selected' : '' }}>@lang('admin.enable')</option>
                    <option value="0" {{ Request::old('status') == 0 ? 'selected' : '' }}>@lang('admin.disable')</option>
                  </select>
                </div>

              </div><!--end .card-body -->
            </div><!--end .card -->
          </div><!--end .col -->
        </div><!--end .row -->
        <!-- END FIELDS -->

        <button class="btn ink-reaction btn-raised btn-primary">@lang('admin.submit')</button>
      </form>

    </div><!--end .section-body -->
  </section>
  <!-- END FORM -->
@endsection
@push('script')
  <script>
    $('body').delegate('.image_input','change', function () {
      var input = this;
      if (input.files && input.files[0]) {
        var key = parseInt($(input).attr('data-key')) + 1;
        var reader = new FileReader();
        reader.onload = function (e) {
          $(input).parent().parent().parent().find('.delete').removeAttr('style');
          $(input).parent().siblings('.image_file').html('<img src="' + e.target.result + '" alt="image">');
          $(input).parent().removeClass('upload-file-container-text');
          $(input).siblings('span').remove();

          var new_input = '<div class="col-sm-6">' +
            '<div class="upload-file-container preview_image">' +
            '<div class="image col-sm-4 text-center">' +
            '<div class="image_file">' +
            '</div>' +
            '<div class="upload-file-container-text">' +
            '<span><i class="md md-add-to-photos"></i> Add image</span>' +
            '<input type="file" name="portfolio_project_image['+ key +'][image]" data-key="'+ key +'" class="image_input"/>' +
            '</div>' +
            '</div>' +
            '<div class="sort col-sm-6">' +
            '<input type="text" name="portfolio_project_image['+ key +'][sort]" placeholder="sort">' +
            '</div>' +
            '<div class="delete col-sm-2" style="display: none">' +
            '<button type="submit" class="btn btn-icon-toggle" data-toggle="tooltip" data-placement="top" data-original-title="Delete image"><i class="fa fa-trash-o"></i></button>' +
            '</div>' +
            '</div>' +
            '</div>';
          $('.block_images').prepend(new_input);
        };
        reader.readAsDataURL(input.files[0]);
      }
    });

    $('body').delegate('.delete button','click', function (e) {
      e.preventDefault();
      console.log($(this).parent().parent())
      $(this).parent().parent().remove();
    });
  </script>
@endpush
