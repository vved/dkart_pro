@extends('admin.layouts.app')

@section('title', 'admin.blog.blog_post.create_post')

@section('content')
  <!-- BEGIN FORM -->
  <section>
    <div class="section-header">
      <ol class="breadcrumb">
        <li class="active">@lang('admin.blog.blog_post.create_post')</li>
      </ol>
    </div>
    <div class="section-body contain-lg">
      <form class="form" method="post" enctype="multipart/form-data" action="{{ route('blog-post.update', $blogPost->id) }}">
      @csrf
      @method('PUT')

      <!-- BEGIN FIELDS -->
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-head">
                <ul class="nav nav-tabs" data-toggle="tabs">
                  @foreach($languages as $key_language => $language)
                    <li class="{{ $loop->first ? 'active' : '' }}"><a
                        href="#lang-{{ $key_language }}">{{ $language }}</a></li>
                  @endforeach
                </ul>
              </div><!--end .card-head -->
              <div class="card-body tab-content">
                @foreach($languages as $key_language => $language)
                  <div class="tab-pane {{ $loop->first ? 'active' : '' }}" id="lang-{{ $key_language }}">

                    <input type="hidden" name="{{ $key_language }}[language]" value="{{ $key_language }}">

                    <div class="form-group floating-label {{ $errors->has($key_language.'.title')? 'has-error' : '' }}">
                      <input type="text" class="form-control" id="{{ $key_language }}-title" name="{{ $key_language }}[title]" value="{{ issetPrint($blogPostDescriptions, $key_language, 'title') }}">
                      <label for="{{ $key_language }}-title">@lang('admin.blog.blog_post.title')</label>
                      @if ($errors->has($key_language.'.title'))
                        <span class="text-danger">{{ $errors->first($key_language.'.title') }}</span>
                      @endif
                    </div>

                    <!-- BEGIN CKEDITOR - STANDARD -->
                    <h4>@lang('admin.blog.blog_post.description')</h4>
                    <div class="form-group floating-label {{ $errors->has($key_language.'.description')? 'has-error' : '' }}">
                      <textarea class="form-control control-12-rows ckeditor" name="{{ $key_language }}[description]" placeholder="Enter text ...">{{ issetPrint($blogPostDescriptions, $key_language, 'description') }}</textarea>
                      @if ($errors->has($key_language.'.description'))
                        <span class="text-danger">{{ $errors->first($key_language.'.description') }}</span>
                      @endif
                    </div>
                    <!-- END CKEDITOR - STANDARD -->

                    <div class="form-group floating-label">
                      <input type="text" class="form-control" id="{{ $key_language }}-meta_title" name="{{ $key_language }}[meta_title]" value="{{ issetPrint($blogPostDescriptions, $key_language, 'meta_title') }}">
                      <label for="{{ $key_language }}-meta_title">@lang('admin.blog.blog_post.meta_title')</label>
                    </div>

                    <div class="form-group floating-label">
                      <textarea class="form-control" id="{{ $key_language }}-meta_description" name="{{ $key_language }}[meta_description]">{{ issetPrint($blogPostDescriptions, $key_language, 'meta_description') }}</textarea>
                      <label for="{{ $key_language }}-meta_description">@lang('admin.blog.blog_post.meta_description')</label>
                    </div>

                    <div class="form-group floating-label">
                      <input type="text" class="form-control" id="{{ $key_language }}-slag" name="{{ $key_language }}[slag]" value="{{ issetPrint($blogPostDescriptions, $key_language, 'slag') }}">
                      <label for="{{ $key_language }}-slag">@lang('admin.blog.blog_post.slag')</label>
                    </div>

                    <div class="form-group floating-label">
                      <input type="text" class="form-control" id="{{ $key_language }}-keyword" name="{{ $key_language }}[keyword]" value="{{ issetPrint($blogPostDescriptions, $key_language, 'keyword') }}">
                      <label for="{{ $key_language }}-keyword">@lang('admin.blog.blog_post.keyword')</label>
                    </div>

                  </div>
                @endforeach
              </div><!--end .card-body -->
            </div><!--end .card -->
          </div><!--end .col -->
        </div><!--end .row -->


        <div class="row">
          <div class="col-md-6">
            <div class="card">
              <div class="card-body">

                <div class="form-group">
                  <label for="category_id">@lang('admin.blog.blog_post.category_id')</label>
                  <select class="form-control select2-list" data-placeholder="Select an item" id="category_id" name="category_id">
                    <option value="0"> </option>
                    @foreach($blogCategories as $category)
                      <option value="{{ $category->blog_category_id }}" {{ $blogPost->category_id == $category->blog_category_id ? 'selected' : '' }}>{{ $category->title }}</option>
                    @endforeach
                  </select>
                </div>

                <div class="form-group floating-label">
                  <input type="text" class="form-control" id="sort_order" name="sort_order" value="{{ $blogPost->sort_order }}">
                  <label for="sort_order">@lang('admin.blog.blog_post.sort_order')</label>
                </div>

                <div class="form-group">
                  <label for="sort_order">@lang('admin.blog.blog_post.status')</label>
                  <select class="form-control" data-placeholder="Select an item" id="status" name="status">
                    <option value="1" {{ $blogPost->status == 1 ? 'selected' : '' }}>@lang('admin.enable')</option>
                    <option value="0" {{ $blogPost->status == 0 ? 'selected' : '' }}>@lang('admin.disable')</option>
                  </select>
                </div>

              </div><!--end .card-body -->
            </div><!--end .card -->
          </div><!--end .col -->

          <div class="col-md-6">
            <div class="card block-blog_post_image">
              <div class="card-head style-primary">
                <header>Image</header>
              </div>
              <div class="card-body no-padding">
                <div class="form-group col-sm-12 block_images">

                  <div class="col-sm-12">
                    <div class="upload-file-container preview_image">
                      <div class="image col-sm-7 text-center">
                        <div class="image_file">
                          @if($blogPost->image)
                            <img alt="image_post" src="{{ $blogPost->image }}" alt="image" width="250">
                          @endif
                        </div>
                        <div class="{{!$blogPost->image?'upload-file-container-text':''}} block-add-image">
                          <span {{$blogPost->image?'style=display:none':''}}><i class="md md-add-to-photos"></i> Add image</span>
                          <input type="file" name="blog_post_image" class="image_input"/>
                        </div>
                      </div>
                      <div class="delete col-sm-25">
                        <button type="submit" class="btn btn-icon-toggle" data-toggle="tooltip" data-original-title="Delete image"><i class="fa fa-trash-o"></i></button>
                      </div>
                    </div>
                  </div>
                </div>
              </div><!--end .card-body -->
            </div><!--end .card -->
          </div><!--end .col -->
        </div><!--end .row -->
        <!-- END FIELDS -->

        <button class="btn ink-reaction btn-raised btn-primary">@lang('admin.submit')</button>
      </form>

    </div><!--end .section-body -->
  </section>
  <!-- END FORM -->
@endsection
@push('script')
  <script>
    $('body').delegate('.image_input','change', function () {
      var input = this;
      if (input.files && input.files[0]) {
        var key = parseInt($(input).attr('data-key')) + 1;
        var reader = new FileReader();
        reader.onload = function (e) {
          $(input).parent().siblings('.image_file').html('<img src="' + e.target.result + '" alt="image">');
          $(input).siblings('span').css('display', 'none');
          $(input).parent().removeClass('upload-file-container-text');
        };
        reader.readAsDataURL(input.files[0]);
      }
    });

    $('body').delegate('.delete button','click', function (e) {
      e.preventDefault();
      console.log($(this).parent().parent().find('.image_input'))
      $(this).parent().parent().find('.image_input').val('');
      $(this).parent().parent().find('.image_file img').remove();
      $(this).parent().parent().find('.block-add-image').addClass('upload-file-container-text');
      $(this).parent().parent().find('.block-add-image span').css('display', 'block');
    });
  </script>
@endpush
