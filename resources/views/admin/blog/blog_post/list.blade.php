@extends('admin.layouts.app')

@section('title', 'admin.blog.blog_post.title')

@section('content')
  <!-- BEGIN TABLE HOVER -->
  <section class="has-actions style-default-bright">
    <div class="section-body_div">
      <br>
      <h2 class="text-primary">@lang('admin.blog.blog_post.title')
      <a href="{{ route('blog-post.create') }}" class="btn ink-reaction btn-raised btn-primary pull-right"><i class="md md-add"></i> New item</a>
      </h2>
      <br>
      <table class="table table-hover">
        <thead>
        <tr>
          <th>@lang('admin.blog.blog_post.id')</th>
          <th>@lang('admin.blog.blog_post.image')</th>
          <th>@lang('admin.blog.blog_post.title')</th>
          <th>@lang('admin.blog.blog_post.sort_order')</th>
          <th>@lang('admin.blog.blog_post.status')</th>
          <th class="text-right">@lang('admin.blog.blog_post.actions')</th>
        </tr>
        </thead>
        <tbody>
        @foreach($blogPosts as $blogPost)
        <tr>
          <td>{{ $blogPost->id }}</td>
          <td><img src="{{ $blogPost->image ? $blogPost->image : 'placeholder.png' }}" alt="image" width="180"></td>
          <td>{{ isset($blogPost->postDescription->first()->title)?$blogPost->postDescription->first()->title:'' }}</td>
          <td>{{ $blogPost->sort_order }}</td>
          <td>{{ $blogPost->status }}</td>
          <td class="text-right">
            <div class="pull-right">
              <form method="POST" action="{{ route('blog-post.destroy', $blogPost->id) }}">
                @csrf
                @method('DELETE')
                <button type="submit" class="btn btn-icon-toggle" data-toggle="tooltip" data-placement="top" data-original-title="Delete row"><i class="fa fa-trash-o"></i></button>
              </form>
            </div>
            <a class="btn btn-icon-toggle pull-right" data-toggle="tooltip" data-placement="top" data-original-title="Edit row" href="{{ route('blog-post.edit', $blogPost->id) }}"><i class="fa fa-pencil"></i></a>
          </td>
        </tr>
        @endforeach
        </tbody>
      </table>
    </div><!--end .section-body -->
  </section>
  <!-- END TABLE HOVER -->
@endsection
