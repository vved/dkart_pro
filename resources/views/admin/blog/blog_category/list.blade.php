@extends('admin.layouts.app')

@section('title', 'admin.blog.blog_category.title')

@section('content')
  <!-- BEGIN TABLE HOVER -->
  <section class="has-actions style-default-bright">
    <div class="section-body_div">
      <br>
      <h2 class="text-primary">@lang('admin.blog.blog_category.title')
      <a href="{{ route('blog-category.create') }}" class="btn ink-reaction btn-raised btn-primary pull-right"><i class="md md-add"></i> New item</a>
      </h2>
      <br>
      <table class="table table-hover">
        <thead>
        <tr>
          <th>@lang('admin.blog.blog_category.id')</th>
          <th>@lang('admin.blog.blog_category.title')</th>
          <th>@lang('admin.blog.blog_category.sort_order')</th>
          <th>@lang('admin.blog.blog_category.status')</th>
          <th class="text-right">@lang('admin.blog.blog_category.actions')</th>
        </tr>
        </thead>
        <tbody>
        @foreach($blogCategories as $blogCategory)
        <tr>
          <td>{{ $blogCategory->id }}</td>
          <td>{{ isset($blogCategory->categoryDescription->first()->title)?$blogCategory->categoryDescription->first()->title:'' }}</td>
          <td>{{ $blogCategory->sort_order }}</td>
          <td>{{ $blogCategory->status }}</td>
          <td class="text-right">
            <div class="pull-right">
              <form method="POST" action="{{ route('blog-category.destroy', $blogCategory->id) }}">
                @csrf
                @method('DELETE')
                <button type="submit" class="btn btn-icon-toggle" data-toggle="tooltip" data-placement="top" data-original-title="Delete row"><i class="fa fa-trash-o"></i></button>
              </form>
            </div>
            <a class="btn btn-icon-toggle pull-right" data-toggle="tooltip" data-placement="top" data-original-title="Edit row" href="{{ route('blog-category.edit', $blogCategory->id) }}"><i class="fa fa-pencil"></i></a>
          </td>
        </tr>
        @endforeach
        </tbody>
      </table>
    </div><!--end .section-body -->
  </section>
  <!-- END TABLE HOVER -->
@endsection
