@extends('admin.layouts.app')

@section('title', 'admin.blog.blog_category.edit_category')

@section('content')
  <!-- BEGIN FORM -->
  <section>
    <div class="section-header">
      <ol class="breadcrumb">
        <li class="active">@lang('admin.blog.blog_category.edit_category')</li>
      </ol>
    </div>
    <div class="section-body contain-lg">
      <form class="form" method="post" action="{{ route('blog-category.update', $blogCategory->id) }}">
      @csrf
      @method('PUT')

      <!-- BEGIN FIELDS -->
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-head">
                <ul class="nav nav-tabs" data-toggle="tabs">
                  @foreach($languages as $key_language => $language)
                    <li class="{{ $loop->first ? 'active' : '' }}"><a href="#lang-{{ $key_language }}">{{ $language }}</a></li>
                  @endforeach
                </ul>
              </div><!--end .card-head -->
              <div class="card-body tab-content">
                @foreach($languages as $key_language => $language)
                  <div class="tab-pane {{ $loop->first ? 'active' : '' }}" id="lang-{{ $key_language }}">

                    <input type="hidden" name="{{ $key_language }}[language]" value="{{ $key_language }}">

                    <div class="form-group floating-label {{ $errors->has($key_language.'.title')? 'has-error' : '' }}">
                      <input type="text" class="form-control" id="{{ $key_language }}-title" name="{{ $key_language }}[title]" value="{{ issetPrint($blogCategoryDescriptions, $key_language, 'title') }}">
                      <label for="{{ $key_language }}-title">@lang('admin.blog.blog_category.title')</label>
                      @if ($errors->has($key_language.'.title'))
                        <span class="text-danger">{{ $errors->first($key_language.'.title') }}</span>
                      @endif
                    </div>

                    <!-- BEGIN CKEDITOR - STANDARD -->
                    <h4>@lang('admin.blog.blog_category.description')</h4>
                    <div class="form-group floating-label {{ $errors->has($key_language.'.description')? 'has-error' : '' }}">
                      <textarea class="form-control control-12-rows ckeditor" name="{{ $key_language }}[description]" placeholder="Enter text ...">{{ issetPrint($blogCategoryDescriptions, $key_language, 'description') }}</textarea>
                      @if ($errors->has($key_language.'.description'))
                        <span class="text-danger">{{ $errors->first($key_language.'.description') }}</span>
                      @endif
                    </div>
                    <!-- END CKEDITOR - STANDARD -->

                    <div class="form-group floating-label">
                      <input type="text" class="form-control" id="{{ $key_language }}-meta_title" name="{{ $key_language }}[meta_title]" value="{{ issetPrint($blogCategoryDescriptions, $key_language, 'meta_title') }}">
                      <label for="{{ $key_language }}-meta_title">@lang('admin.blog.blog_category.meta_title')</label>
                    </div>

                    <div class="form-group floating-label">
                      <textarea class="form-control" id="{{ $key_language }}-meta_description" name="{{ $key_language }}[meta_description]" value="{{ issetPrint($blogCategoryDescriptions, $key_language, 'meta_description') }}"></textarea>
                      <label for="{{ $key_language }}-meta_description">@lang('admin.blog.blog_category.meta_description')</label>
                    </div>

                    <div class="form-group floating-label {{ $errors->has($key_language.'.slag')? 'has-error' : '' }}">
                      <input type="text" class="form-control" id="{{ $key_language }}-slag" name="{{ $key_language }}[slag]" value="{{ issetPrint($blogCategoryDescriptions, $key_language, 'slag') }}">
                      <label for="{{ $key_language }}-slag">@lang('admin.blog.blog_category.slag')</label>
                      @if ($errors->has($key_language.'.slag'))
                        <span class="text-danger">{{ $errors->first($key_language.'.slag') }}</span>
                      @endif
                    </div>

                    <div class="form-group floating-label">
                      <input type="text" class="form-control" id="{{ $key_language }}-keyword" name="{{ $key_language }}[keyword]" value="{{ issetPrint($blogCategoryDescriptions, $key_language, 'keyword') }}">
                      <label for="{{ $key_language }}-keyword">@lang('admin.blog.blog_category.keyword')</label>
                    </div>

                  </div>
                @endforeach
              </div><!--end .card-body -->
            </div><!--end .card -->
          </div><!--end .col -->
        </div><!--end .row -->


        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-body">

                <div class="form-group">
                  <label for="sort_order">@lang('admin.blog.blog_category.parent_id')</label>
                  <select class="form-control select2-list" data-placeholder="Select an item" id="parent_id" name="parent_id">
                    <option value="0"></option>
                    @foreach($blogCategories as $category)
                      @if($blogCategory->id != $category->blog_category_id)
                        <option value="{{ $category->blog_category_id }}" {{ $blogCategory->parent_id == $category->blog_category_id ? 'selected' : '' }}>{{ $category->title }}</option>
                      @endif
                    @endforeach
                  </select>
                </div>

                <div class="form-group floating-label">
                  <input type="text" class="form-control" id="sort_order" name="sort_order" value="{{ $blogCategory->sort_order }}">
                  <label for="sort_order">@lang('admin.blog.blog_category.sort_order')</label>
                </div>

                <div class="form-group">
                  <label for="sort_order">@lang('admin.blog.blog_category.status')</label>
                  <select class="form-control" data-placeholder="Select an item" id="status" name="status">
                    <option value="1" {{ $blogCategory->status == 1 ? 'selected' : '' }}>@lang('admin.enable')</option>
                    <option value="0" {{ $blogCategory->status == 0 ? 'selected' : '' }}>@lang('admin.disable')</option>
                  </select>
                </div>
              </div><!--end .card-body -->
            </div><!--end .card -->
          </div><!--end .col -->
        </div><!--end .row -->
        <!-- END FIELDS -->

        <button class="btn ink-reaction btn-raised btn-primary">@lang('admin.submit')</button>
      </form>

    </div><!--end .section-body -->
  </section>
  <!-- END FORM -->
@endsection
