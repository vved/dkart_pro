@php
$config = [
    'appName' => config('app.name'),
    'locale' => $locale = app()->getLocale(),
    'locales' => config('app.locales'),
    'githubAuth' => config('services.github.client_id'),
];
@endphp
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'>
  <title>{{ config('app.name') }}</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="author" content="Kashavtsev Denis">
  <meta name="yandex-verification" content="b80cb1bcce5c221d" />
  <link rel="shortcut icon" href="/dist/img/ico/favicon.ico">
  <link href="{{ mix('dist/css/app.css') }}" rel="stylesheet">
  <link rel="manifest" href="manifest.json">
</head>

<body id="page-top" class="index">
<div id="app"></div>

{{-- Global configuration object --}}
<script>
  window.config = @json($config);
</script>
{{-- Load the application scripts --}}
<script src="{{ mix('dist/js/app.js') }}"></script>
{{--<script src="/service-worker.js"></script>--}}
</body>

</html>

