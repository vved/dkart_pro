import Vue from 'vue'
import Card from './elements/Card'
import Child from './elements/Child'
import Button from './elements/Button'
import Checkbox from './elements/Checkbox'
import ModalProject from './ModalProject'
import { HasError, AlertError, AlertSuccess } from 'vform'

// Components that are registered globaly.
[
  Card,
  Child,
  Button,
  Checkbox,
  ModalProject,
  HasError,
  AlertError,
  AlertSuccess
].forEach(Component => {
  Vue.component(Component.name, Component)
})
