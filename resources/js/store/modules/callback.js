import axios from 'axios'

// state
export const state = {
  status: []
},modules = {
  lang: 'lang'
}

// getters
export const getters = {
  status: state => state.status
}

// mutations
export const mutations = {
  setStatus: (state, status) => {
    state.status = status
  }
}

// actions
export const actions = {
  async sendMessage ({ commit }, fields) {
    axios.post('/api/send-message', { fields, lang: this.state.lang.locale }).then(response => {
      commit('setStatus', response.data )
    }).catch(error => {
      if (error.response) {
        commit('setStatus', error.response.data )
      }
    });
  }
}
