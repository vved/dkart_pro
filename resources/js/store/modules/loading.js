// state
import axios from "axios";

export const state = {
  loading: true
}

// getters
export const getters = {
  loading: state => state.loading
}

// mutations
export const mutations = {
  setLoading (state, loading) {
    state.loading = loading
  },
}

// actions
export const actions = {
  async setLoading({commit}, value=false) {
    commit('setLoading', value)
  },
}
