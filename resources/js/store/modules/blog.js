import axios from 'axios'
// state
export const state = {
  posts: [],
  post: [],
  categories: [],
},modules = {
  lang: 'lang',
  loading: 'loading'
}

// getters
export const getters = {
  posts: state => state.posts,
  post: state => state.post,
  categories: state => state.categories
}

// mutations
export const mutations = {
  setPosts (state, posts) {
    state.posts = posts
  },
  setPost (state, post) {
    state.post = post
  },
  setCategories (state, categories) {
    state.categories = categories
  }
}

// actions
export const actions = {
  async getPosts ({ commit }, category_id = null) {
    this.state.loading.loading = true
    axios.get('/api/blog-posts', { params: { limit: 10, lang: this.state.lang.locale, category_id: category_id } }).then(response => {
      commit('setPosts', response.data )
      this.state.loading.loading = false
    });
  },
  async getPost ({ commit }, id) {
    this.state.loading.loading = true
    axios.get('/api/blog-post', { params: { limit: 10, lang: this.state.lang.locale, id: id } }).then(response => {
      commit('setPost', response.data )
      this.state.loading.loading = false
    });
  },
  async getCategories ({ commit }) {
    this.state.loading.loading = true
    axios.get('/api/blog-categories', { params: {lang: this.state.lang.locale } }).then(response => {
      commit('setCategories', response.data )
      this.state.loading.loading = false
    });
  }
}
