import axios from 'axios'
// state
export const state = {
  projects: [],
  project: []
},modules = {
  lang: 'lang',
  loading: 'loading'
}

// getters
export const getters = {
  projects: state => state.projects,
  project: state => state.project
}

// mutations
export const mutations = {
  setProjects (state, projects) {
    state.projects = projects
  },
  setProject (state, project) {
    state.project = project
  },
}

// actions
export const actions = {
  async getProjects ({ commit }) {
    this.state.loading.loading = true
    axios.get('/api/projects', { params: { limit: 10, lang: this.state.lang.locale } }).then(response => {
      commit('setProjects', response.data )
      this.state.loading.loading = false
    });
  },
  async getProject ({ commit }, id) {
    this.state.loading.loading = true
    axios.get('/api/project', { params: { limit: 10, lang: this.state.lang.locale, id: id} }).then(response => {
      commit('setProject', response.data )
      this.state.loading.loading = false
    });
  }
}
