import Vue from 'vue'
import store from '~/store'
import router from '~/router'
import i18n from '~/plugins/i18n'
import App from '~/components/App'
import AOS from 'aos';
import {VueMasonryPlugin} from 'vue-masonry';

import '~/plugins'
import '~/components'
import '~/registerServiceWorker'

Vue.config.productionTip = false

AOS.init();

Vue.use(VueMasonryPlugin)

new Vue({
  i18n,
  store,
  router,
  ...App
})
