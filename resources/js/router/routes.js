function page (path) {
  return () => import(/* webpackChunkName: '' */ `~/pages/${path}`).then(m => m.default || m)
}

export default [
  { path: '/', name: 'home', component: page('Home.vue') },
  { path: '/blog/:category_id?', name: 'blog', component: page('Blog.vue') },
  { path: '/blog/post/:id', name: 'blogPost', component: page('BlogPost.vue') },

  { path: '/project/:id', name: 'project', component: page('Project.vue') },
  { path: '/cv', name: 'cv', component: page('Cv.vue') },

  { path: '*', component: page('errors/404.vue') }
]
