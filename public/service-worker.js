const staticCacheName = 'static-v0'
const dynamicCacheName = 'dynamic-v0'

const assetUrls = [
  '/',
  '/dist/css/app.css',
  '/dist/img/logo/dk-black.svg',
  '/dist/img/logo/dk.svg',
  '/dist/js/app.js',
  '/images/bg1.jpg',
  'offline.html'
]

self.addEventListener('install', async event => {
  const cache = await caches.open(staticCacheName)
  await cache.addAll(assetUrls)
})

self.addEventListener('activate', async event => {
  const cacheNames = await caches.keys()
  await Promise.all(
    cacheNames
      .filter(name => name !== staticCacheName)
      .filter(name => name !== dynamicCacheName)
      .map(name => caches.delete(name))
  )
})

self.addEventListener('fetch', event => {
  const {request} = event

  const url = new URL(request.url)
  if (url.origin === location.origin) {
    event.respondWith(cacheFirst(request))
  } else {
    event.respondWith(networkFirst(request))
  }
})


async function cacheFirst(request) {
  const cached = await caches.match(request)
  return cached ?? await fetch(request)
}

async function networkFirst(request) {
  const cache = await caches.open(dynamicCacheName)
  try {
    const response = await fetch(request)
    await cache.put(request, response.clone())
    return response
  } catch (e) {
    const cached = await cache.match(request)
    return cached ?? await caches.match('/offline.html')
  }
}


// let deferredPrompt;
// let btnAdd = document.getElementById('install_app');
//
// self.addEventListener('beforeinstallprompt', function(event) {
//   // Prevent Chrome 67 and earlier from automatically showing the prompt
//   event.preventDefault();
//   // Stash the event so it can be triggered later.
//   deferredPrompt = event;
// });

// // Installation must be done by a user gesture! Here, the button click
// btnAdd.addEventListener('click', (e) => {
//   // hide our user interface that shows our A2HS button
//   // btnAdd.style.display = 'none';
//   // Show the prompt
//   deferredPrompt.prompt();
//   // Wait for the user to respond to the prompt
//   deferredPrompt.userChoice
//     .then((choiceResult) => {
//       if (choiceResult.outcome === 'accepted') {
//         console.log('User accepted the A2HS prompt');
//       } else {
//         console.log('User dismissed the A2HS prompt');
//       }
//       deferredPrompt = null;
//     });
// });
