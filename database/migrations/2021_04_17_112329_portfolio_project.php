<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PortfolioProject extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('portfolio_project', function (Blueprint $table) {
            $table->id();
            $table->integer('sort_order')->default('0')->nullable();
            $table->integer('status')->nullable();
            $table->timestamps();
        });

        Schema::create('portfolio_project_description', function (Blueprint $table) {
            $table->id();
            $table->integer('portfolio_project_id')->default('0');
            $table->string('language')->default(Config::get('app.locale'));
            $table->string('title')->nullable();
            $table->text('description')->nullable();
            $table->string('meta_title')->nullable();
            $table->text('meta_description')->nullable();
            $table->string('keyword')->nullable();
            $table->string('slag')->unique();
            $table->timestamps();
        });

        Schema::create('portfolio_project_image', function (Blueprint $table) {
            $table->id();
            $table->integer('portfolio_project_id')->default('0');
            $table->string('image');
            $table->integer('sort_order')->default('0')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('portfolio_project');
        Schema::dropIfExists('portfolio_project_description');
        Schema::dropIfExists('portfolio_project_image');
    }
}
