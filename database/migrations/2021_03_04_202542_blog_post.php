<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class BlogPost extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blog_post', function (Blueprint $table) {
            $table->id();
            $table->integer('category_id')->default('0');
            $table->integer('sort_order')->default('0')->nullable();
            $table->integer('status')->nullable();
            $table->timestamps();
        });

        Schema::create('blog_post_description', function (Blueprint $table) {
            $table->id();
            $table->integer('blog_post_id')->default('0');
            $table->string('language')->default(Config::get('app.locale'));
            $table->string('title')->nullable();
            $table->text('description')->nullable();
            $table->string('meta_title')->nullable();
            $table->text('meta_description')->nullable();
            $table->string('keyword')->nullable();
            $table->string('slag')->unique();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blog_post');
        Schema::dropIfExists('blog_post_description');
    }
}
