<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\BlogCategory;
use App\Models\BlogCategoryDescription;
use App\Models\BlogPost;
use App\Models\BlogPostDescription;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Validation\Rule;

class BlogCategoryController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCategories(Request $request): \Illuminate\Http\JsonResponse
    {
        $blogCategories = BlogCategory::with(['categoryDescriptionFirst' => function ($q) use ($request){
            $q->where('language', $request->lang);
        }])->where('status', 1)->limit($request->limit)->get();

        return response()->json($blogCategories);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCategory(Request $request): \Illuminate\Http\JsonResponse
    {
        $blogCategory = BlogCategory::with(['categoryDescriptionFirst' => function ($q) use ($request){
            $q->where('language', $request->lang);
        }])->where(['id' => $request->id, 'status' => 1])->first();

        return response()->json($blogCategory);
    }
}
