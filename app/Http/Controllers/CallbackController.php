<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\CallbackRequest;
use App\Mail\CallBack;
use App\Models\PortfolioProject;
use App\Models\PortfolioProjectDescription;
use App\Models\PortfolioProjectImage;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Mail;
use Illuminate\Validation\Rule;

class CallbackController extends Controller
{
    public function sendMessage(CallbackRequest $request){
        App::setLocale($request->lang);

        Mail::to('dkashavtsev@gmail.com')->send(new CallBack($request->fields));

        return response()->json(['success'=>true]);
    }
}
