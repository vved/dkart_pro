<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\BlogCategoryDescription;
use App\Models\BlogPost;
use App\Models\BlogPostDescription;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Validation\Rule;

class BlogPostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $languages = config('app.locales');

        $blogPosts = BlogPost::with(['postDescription'=> function ($q){
            $q->where('language', App::getLocale());
        }])->paginate(25);

        return view('admin.blog.blog_post.list', compact('blogPosts', 'languages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $blogCategories = BlogCategoryDescription::where('language', App::getLocale())->select('title', 'blog_category_id')->get();

        $languages = config('app.locales');

        return view('admin.blog.blog_post.create', compact('languages', 'blogCategories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $languages = config('app.locales');

        $rules = array();

        foreach ($languages as $keyLanguage => $language){
            $rules[$keyLanguage.'.title'] = 'required';
            $rules[$keyLanguage.'.description'] = 'required';
            $rules[$keyLanguage.'.slag'] = 'unique:blog_category_description,slag';
        }
        $request->validate($rules);

        $blogPost = BlogPost::create($request->all());

        if($request->hasFile('blog_post_image')){
            $file = $request->file('blog_post_image');
            $fileName = $file->getClientOriginalName();
            $path = public_path() . '/storage/uploads/blog_files/';
            $file->move($path, $fileName);
            $blogPost->image = '/storage/uploads/blog_files/' . $fileName;
            $blogPost->save();
        }

        foreach ($languages as $keyLanguage => $language) {
            $blogPostDescription[] = new BlogPostDescription($request->$keyLanguage);
        }

        if(!empty($blogPostDescription)) {
            $blogPost->postDescription()->saveMany($blogPostDescription);
        }

        $request->session()->flash('status', 'BlogPost successful!');

        return redirect()->route('blog-post');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\BlogPost  $blogPost
     * @return \Illuminate\Http\Response
     */
    public function show(BlogPost $blogPost)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\BlogPost  $blogPost
     * @return \Illuminate\Http\Response
     */
    public function edit(BlogPost $blogPost)
    {
        $blogCategories = BlogCategoryDescription::where('language', App::getLocale())->select('title', 'blog_category_id')->get();

        $blogPostDescriptions = $blogPost->postDescription()->get()->keyBy('language');

        $languages = config('app.locales');

        return view('admin.blog.blog_post.edit', compact('blogPost', 'blogPostDescriptions', 'languages', 'blogCategories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\BlogPost  $blogPost
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BlogPost $blogPost)
    {
        $languages = config('app.locales');

        $rules = array();

        foreach ($languages as $keyLanguage => $language){
            $description = $blogPost->postDescription()->where(['language' => $keyLanguage])->first();

            $rules[$keyLanguage.'.title'] = 'required';
            $rules[$keyLanguage.'.description'] = 'required';
            $rules[$keyLanguage.'.slag'] = Rule::unique('blog_post_description', 'slag')->ignore(isset($description->id)?$description->id:0);
        }

        $request->validate($rules);

        if($request->hasFile('blog_post_image')){
            $file = $request->file('blog_post_image');
            $fileName = $file->getClientOriginalName();
            $path = public_path() . '/storage/uploads/blog_files/';
            $file->move($path, $fileName);
            $blogPost->image = '/storage/uploads/blog_files/' . $fileName;
        }
        $blogPost->update($request->all());

        $blogPost->postDescription()->delete();

        foreach ($languages as $keyLanguage => $language) {
            $blogPostDescription[] = new BlogPostDescription($request->$keyLanguage);
        }

        if(!empty($blogPostDescription)) {
            $blogPost->postDescription()->saveMany($blogPostDescription);
        }

        $request->session()->flash('status', 'BlogPost successful!');

        return redirect()->route('blog-post');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\BlogPost  $blogPost
     * @return \Illuminate\Http\Response
     */
    public function destroy(BlogPost $blogPost)
    {
        $blogPost->postDescription()->delete();

        $blogPost->delete();

        request()->session()->flash('status', 'BlogPost successful!');

        return redirect()->route('blog-post');
    }
}
