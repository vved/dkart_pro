<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\PortfolioProject;
use App\Models\PortfolioProjectDescription;
use App\Models\PortfolioProjectImage;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Validation\Rule;

class PortfolioProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $languages = config('app.locales');

        $portfolioProjects = PortfolioProject::with(['portfolioDescription' => function ($q){
            $q->where('language', App::getLocale());
        }, 'portfolioImage' => function($q){
            $q->orderBy('sort_order');
        }])->orderBy('sort_order')
            ->paginate(25);

        return view('admin.portfolio.list', compact('portfolioProjects', 'languages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $languages = config('app.locales');

        return view('admin.portfolio.create', compact('languages'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $languages = config('app.locales');

        $rules = array();

        foreach ($languages as $keyLanguage => $language){
            $rules[$keyLanguage.'.title'] = 'required';
            $rules[$keyLanguage.'.description'] = 'required';
            $rules[$keyLanguage.'.slag'] = 'unique:blog_category_description,slag';
        }
        $request->validate($rules);

        $portfolioProject = PortfolioProject::create($request->all());

        if(isset($request->portfolio_project_image)) {
            foreach ($request->portfolio_project_image as $image) {
                if(isset($image['image'])) {
                    $file = $image['image'];
                    $fileName = $file->getClientOriginalName();
                    $path = public_path() . '/storage/uploads/blog_files/';
                    $file->move($path, $fileName);
                    $portfolioProjectImage[] = new PortfolioProjectImage([
                        'image' => '/storage/uploads/blog_files/' . $fileName,
                        'sort_order' => $image['sort']
                    ]);
                }
            }
        }

        if(!empty($portfolioProjectImage)) {
            $portfolioProject->portfolioImage()->saveMany($portfolioProjectImage);
        }

        foreach ($languages as $keyLanguage => $language) {
            $portfolioProjectDescription[] = new PortfolioProjectDescription($request->$keyLanguage);
        }

        if(!empty($portfolioProjectDescription)) {
            $portfolioProject->portfolioDescription()->saveMany($portfolioProjectDescription);
        }

        $request->session()->flash('status', 'Portfolio successful!');

        return redirect()->route('portfolio-project');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PortfolioProject  $portfolioProject
     * @return \Illuminate\Http\Response
     */
    public function show(PortfolioProject $portfolioProject)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PortfolioProject  $portfolioProject
     * @return \Illuminate\Http\Response
     */
    public function edit(PortfolioProject $portfolioProject)
    {
        $portfolioProjectDescriptions = $portfolioProject->portfolioDescription()->get()->keyBy('language');

        $portfolioProjectImages = $portfolioProject->portfolioImage()->get();

        $key_image = $portfolioProjectImages->count();

        $languages = config('app.locales');

        return view('admin.portfolio.edit', compact('portfolioProject', 'portfolioProjectDescriptions', 'languages', 'portfolioProjectImages', 'key_image'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PortfolioProject  $portfolioProject
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PortfolioProject $portfolioProject)
    {
        $languages = config('app.locales');

        $rules = array();

        foreach ($languages as $keyLanguage => $language){
            $description = $portfolioProject->portfolioDescription()->where(['language' => $keyLanguage])->first();

            $rules[$keyLanguage.'.title'] = 'required';
            $rules[$keyLanguage.'.description'] = 'required';
            $rules[$keyLanguage.'.slag'] = Rule::unique('blog_post_description', 'slag')->ignore(isset($description->id)?$description->id:0);
        }

        $request->validate($rules);

        $portfolioProject->update($request->all());

        $portfolioProject->portfolioImage()->delete();

        if(isset($request->portfolio_project_image)) {
            foreach ($request->portfolio_project_image as $image) {

                if(isset($image['image'])) {
                    $pathFileName = '';
                    if(!is_string($image['image'])){
                        $file = $image['image'];
                        $fileName = $file->getClientOriginalName();
                        $path = public_path() . '/storage/uploads/blog_files/';
                        $file->move($path, $fileName);
                        $pathFileName = '/storage/uploads/blog_files/' . $fileName;
                    }

                    $portfolioProjectImage[] = new PortfolioProjectImage([
                        'image' => $pathFileName?:$image['image'],
                        'sort_order' => $image['sort']
                    ]);
                }
            }
        }

        if(!empty($portfolioProjectImage)) {
            $portfolioProject->portfolioImage()->saveMany($portfolioProjectImage);
        }

        $portfolioProject->portfolioDescription()->delete();

        foreach ($languages as $keyLanguage => $language) {
            $portfolioProjectDescription[] = new PortfolioProjectDescription($request->$keyLanguage);
        }

        if(!empty($portfolioProjectDescription)) {
            $portfolioProject->portfolioDescription()->saveMany($portfolioProjectDescription);
        }

        $request->session()->flash('status', 'BlogPost successful!');

        return redirect()->route('portfolio-project');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PortfolioProject  $portfolioProject
     * @return \Illuminate\Http\Response
     */
    public function destroy(PortfolioProject $portfolioProject)
    {
        $portfolioProject->portfolioDescription()->delete();

        $portfolioProject->delete();

        request()->session()->flash('status', 'Portfolio successful!');

        return redirect()->route('portfolio-project');
    }
}
