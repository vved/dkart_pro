<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\BlogCategory;
use App\Models\BlogCategoryDescription;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Validation\Rule;
use phpDocumentor\Reflection\Location;

class BlogCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function index()
    {
        $languages = config('app.locales');

        $blogCategories = BlogCategory::with(['categoryDescription'=> function ($q){
            $q->where('language', App::getLocale());
        }])->paginate(25);

        return view('admin.blog.blog_category.list', compact('blogCategories', 'languages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function create()
    {
        $blogCategories = BlogCategoryDescription::where('language', App::getLocale())->select('title', 'blog_category_id')->get();

        $languages = config('app.locales');

        return view('admin.blog.blog_category.create', compact('languages', 'blogCategories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $languages = config('app.locales');

        $rules = array();

        foreach ($languages as $keyLanguage => $language){
            $rules[$keyLanguage.'.title'] = 'required';
            $rules[$keyLanguage.'.description'] = 'required';
            $rules[$keyLanguage.'.slag'] = 'unique:blog_category_description,slag';
        }
        $request->validate($rules);

        $blogCategory = BlogCategory::create($request->all());

        foreach ($languages as $keyLanguage => $language) {
            $blogCategoryDescription[] = new BlogCategoryDescription($request->$keyLanguage);
        }

        if(!empty($blogCategoryDescription)) {
            $blogCategory->categoryDescription()->saveMany($blogCategoryDescription);
        }

        $request->session()->flash('status', 'BlogCategory successful!');

        return redirect()->route('blog-category');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\BlogCategory  $blogCategory
     * @return \Illuminate\Http\Response
     */
    public function show(BlogCategory $blogCategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\BlogCategory  $blogCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(BlogCategory $blogCategory)
    {
        $blogCategories = BlogCategoryDescription::where('language', App::getLocale())->select('title', 'blog_category_id')->get();

        $blogCategoryDescriptions = $blogCategory->categoryDescription()->get()->keyBy('language');

        $languages = config('app.locales');

        return view('admin.blog.blog_category.edit', compact('blogCategory', 'blogCategoryDescriptions', 'languages', 'blogCategories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\BlogCategory $blogCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BlogCategory $blogCategory)
    {
        $languages = config('app.locales');

        $rules = array();

        foreach ($languages as $keyLanguage => $language){
            $description = $blogCategory->categoryDescription()->where(['language' => $keyLanguage])->first();

            $rules[$keyLanguage.'.title'] = 'required';
            $rules[$keyLanguage.'.description'] = 'required';
            $rules[$keyLanguage.'.slag'] = Rule::unique('blog_category_description', 'slag')->ignore($description->id);
        }

        $request->validate($rules);

        $blogCategory->update($request->all());

        $blogCategory->categoryDescription()->delete();

        foreach ($languages as $keyLanguage => $language) {
            $blogCategoryDescription[] = new BlogCategoryDescription($request->$keyLanguage);
        }

        if(!empty($blogCategoryDescription)) {
            $blogCategory->categoryDescription()->saveMany($blogCategoryDescription);
        }

        $request->session()->flash('status', 'BlogCategory successful!');

        return redirect()->route('blog-category');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\BlogCategory  $blogCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(BlogCategory $blogCategory)
    {
        $blogCategory->categoryDescription()->delete();

        $blogCategory->delete();

        request()->session()->flash('status', 'BlogCategory successful!');

        return redirect()->route('blog-category');
    }
}
