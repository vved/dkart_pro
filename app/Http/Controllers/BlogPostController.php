<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\BlogCategoryDescription;
use App\Models\BlogPost;
use App\Models\BlogPostDescription;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Validation\Rule;

class BlogPostController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPosts(Request $request): \Illuminate\Http\JsonResponse
    {
        $blogPosts = BlogPost::with(['postDescriptionFirst' => function ($q) use ($request){
            $q->where('language', $request->lang);
        }])->where('status', 1);

        if(isset($request->category_id)){
            $blogPosts = $blogPosts->where('category_id', $request->category_id);
        }

        $blogPosts = $blogPosts->limit($request->limit)->get();

        return response()->json($blogPosts);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPost(Request $request): \Illuminate\Http\JsonResponse
    {
        $blogPost = BlogPost::with(['postDescriptionFirst' => function ($q) use ($request){
            $q->where('language', $request->lang);
        }])->where(['id' => $request->id, 'status' => 1])->first();

        return response()->json($blogPost);
    }
}
