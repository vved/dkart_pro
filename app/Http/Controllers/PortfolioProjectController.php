<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\PortfolioProject;
use App\Models\PortfolioProjectDescription;
use App\Models\PortfolioProjectImage;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Validation\Rule;

class PortfolioProjectController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getProjects(Request $request){

        $portfolioProjects = PortfolioProject::with(['portfolioDescriptionFirst' => function ($q) use ($request){
            $q->where('language', $request->lang);
        }, 'portfolioImageFirst' => function($q){
            $q->orderBy('sort_order');
        }])->where('status', 1)
            ->limit($request->limit)
            ->orderBy('sort_order')
            ->get();

        return response()->json($portfolioProjects);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getProject(Request $request){

        $portfolioProject = PortfolioProject::with(['portfolioDescriptionFirst' => function ($q) use ($request){
            $q->where('language', $request->lang);
        }, 'portfolioImage'])->where(['id' => $request->id, 'status' => 1])->first();

        return response()->json($portfolioProject);
    }
}
