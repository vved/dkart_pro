<?php

namespace App\Http\Requests;

use App\Exceptions\ApiValidationException;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\ValidationException;

class CallbackRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fields.name' => 'required',
            'fields.mail' => 'required',
            'fields.message' => 'required'
        ];
    }

    /**
     * Modify input data
     *
     * @return array
     */
    public function getSanitized(): array
    {
        $sanitized = $this->validated();

        return $sanitized;
    }

    /**
     * @param Validator $validator
     * @throws ValidationException
     */
    protected function failedValidation(Validator $validator)
    {
        if ($this->expectsJson()) {
            $response_error = array();
            $errors = (new ValidationException($validator))->errors();

            foreach ($errors as $key => $error){
                $response_error[] = $key;
            }

            throw new HttpResponseException(
                response()->json(['errors' => $response_error], 422)
            );
        }

        parent::failedValidation($validator);
    }
}
