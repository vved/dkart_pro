<?php

if (! function_exists('issetPrint')) {
    /**
     * @param $value
     * @return mixed|string
     */
    function issetPrint($object, $index, $value): string
    {
        return isset($object[$index]->$value) ? $object[$index]->$value : '';
    }
}
