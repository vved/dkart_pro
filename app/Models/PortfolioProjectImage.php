<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PortfolioProjectImage extends Model
{
    use HasFactory;

    public $table = 'portfolio_project_image';

    protected $fillable = [
        'portfolio_project_id',
        'image',
        'sort_order'
    ];

    public function getImageAttribute($value){
        return $value;
    }
}
