<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class BlogCategory extends Model
{
    use HasFactory;

    protected $table = 'blog_category';

    protected $fillable = [
        'parent_id',
        'sort_order',
        'status'
    ];

    /**
     * @return HasMany
     */
    public function categoryDescription(): HasMany
    {
        return $this->hasMany(BlogCategoryDescription::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function categoryDescriptionFirst(): HasOne
    {
        return $this->hasOne(BlogCategoryDescription::class);
    }
}
