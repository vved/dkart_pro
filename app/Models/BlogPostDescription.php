<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class BlogPostDescription extends Model
{
    use HasFactory;

    protected $table = 'blog_post_description';

    protected $fillable = [
        'blog_post_id',
        'language',
        'title',
        'description',
        'meta_title',
        'meta_description',
        'keyword',
        'slag'
    ];

    protected $appends = [
        'short_description'
    ];

    /**
     * @param $value
     */
    public function setSlagAttribute($value){
        if(!$value){
            $check_exist = $this->where('slag', Str::slug($this->attributes['title']))->first();
            if(!$check_exist) {
                $this->attributes['slag'] = Str::slug($this->attributes['title']);
            } else {
                $this->attributes['slag'] = Str::slug($this->attributes['title']).'-'.$this->attributes['language'].'-'.time();
            }
        } else {
            $this->attributes['slag'] = $value;
        }
    }

    /**
     * @return false|string
     */
    public function getShortDescriptionAttribute(){
        return substr(strip_tags($this->description), 0, 200);
    }

}
