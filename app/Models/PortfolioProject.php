<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PortfolioProject extends Model
{
    use HasFactory;

    public $table = 'portfolio_project';

    protected $fillable = [
        'sort_order',
        'status'
    ];

    public function portfolioDescription(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(PortfolioProjectDescription::class);
    }

    public function portfolioDescriptionFirst(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(PortfolioProjectDescription::class);
    }

    public function portfolioImage(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(PortfolioProjectImage::class);
    }

    public function portfolioImageFirst(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(PortfolioProjectImage::class);
    }
}
