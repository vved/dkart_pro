<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Str;

class BlogCategoryDescription extends Model
{
    use HasFactory;

    protected $table = 'blog_category_description';

    protected $fillable = [
        'blog_category_id',
        'language',
        'title',
        'description',
        'meta_title',
        'meta_description',
        'keyword',
        'slag'
    ];

    public function setSlagAttribute($value){
        if(!$value){
            $check_exist = $this->where('slag', Str::slug($this->attributes['title']))->first();
            if(!$check_exist) {
                $this->attributes['slag'] = Str::slug($this->attributes['title']);
            } else {
                $this->attributes['slag'] = Str::slug($this->attributes['title']).'-'.$this->attributes['language'].'-'.time();
            }
        } else {
            $this->attributes['slag'] = $value;
        }
    }
}
