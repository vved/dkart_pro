<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class BlogPost extends Model
{
    use HasFactory;

    protected $table = 'blog_post';

    protected $fillable = [
        'category_id',
        'sort_order',
        'status',
        'image'
    ];

    /**
     * @return HasMany
     */
    public function postDescription(): HasMany
    {
        return $this->hasMany(BlogPostDescription::class);
    }

    /**
     * @return HasOne
     */
    public function postDescriptionFirst(): HasOne
    {
        return $this->hasOne(BlogPostDescription::class);
    }

}
